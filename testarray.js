const arrays = require("./array.js");

let array = new arrays();

const items = [1, 2, 3, 4, 5, 5]; 

 array.each(items, (element) => { console.log(`element = ${element}`) } );

array.each(items, (element, index) => { console.log(`element = ${element}, index = ${index}`) } );

array.each(items, (element, index, array) => { console.log(`element = ${element}, index = ${index}, array = [${array}]`) } );
 



/* let map1 = array.map(items, (element) => { return element } );
console.log(map1);

let map2 = array.map(items, (element, index) => { return element * index } );
console.log(map2); */




/* let reduce1 = array.reduce(items, (accumulator, currentValue) => { return accumulator + currentValue },  0);
console.log(reduce1);

let reduce2 = array.reduce(["a", "b", "c"], (element, index) => { return element + index } );
console.log(reduce2);

let reduce3 = array.reduce(items, (element, index) => { return element + index } );
console.log(reduce3); */




/* let find1 = array.find(["a", "b", "c"], (element, index) => { return element === "a" } );
console.log(find1);

let find2 = array.find( (element, index) => { return element + index } );
console.log(find2);

 */





/* 
let filter1 = array.filter(items, (element) => { return element == 5 } );
console.log(filter1); */







/* const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'

let flatten1 = array.flatten(nestedArray, 1);
console.log(flatten1); */






