/* function counterFactory() {
  // Return an Object that has two methods called `increment` and `decrement`.
  // `increment` should increment a counter variable in closure scope and return it.
  // `decrement` should decrement the counter variable and return it.
}
 */

const Closures = function () {};

Closures.prototype.counterFactory = function () {
  let counter = 0;
  let Obj = {};

  Obj.increment = function () {
    counter++;
    return counter;
  };
  Obj.decrement = function () {
    counter--;
    return counter;
  };

  return Obj;
};




/* function limitFunctionCallCount(cb, n) {
  // Should return a function that invokes `cb`.
  // The returned function should only allow `cb` to be invoked `n` times.
  // Returning null is acceptable if cb can't be returned
} */

/* 
Closures.prototype.limitFunctionCallCount = function(cb, n){

    let count = 0;

    return function() {
        var arg = Object.values(arguments);
        
        if (count < n) {
            count++;
            return cb(...arg);
        }
    }
    
} */






/* function cacheFunction(cb) {
  // Should return a function that invokes `cb`.
  // A cache (object) should be kept in closure scope.
  // The cache should keep track of all arguments have been used to invoke this function.
  // If the returned function is invoked with arguments that it has already seen
  // then it should return the cached result and not invoke `cb` again.
  // `cb` should only ever be invoked once for a given set of arguments.
} */



/* 

Closures.prototype.cacheFunction = function(cb){

    let cache = {};
    
    return function() {
        var Arg = Object.values(arguments);
        
		if (cache.hasOwnProperty(Arg.toString())) {
            console.log("from cache")
            
			return cache[Arg]
		} else {
            console.log('Calculating result');
            let result = cb(...Arg);
            
			return cache[Arg] = result;
		}
	}
  
} */



module.exports = Closures;
