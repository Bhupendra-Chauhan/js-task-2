/* function keys(obj) {
  // Retrieve all the names of the object's properties.
  // Return the keys as strings in an array.
  // Based on http://underscorejs.org/#keys
}
 */


const Objects = function(){

}

Objects.prototype.keys = function(obj){
  
  let keys = [];
  for (let i in obj) {
    keys.push(i);
  }
  return keys;

}




/* function values(obj) {
  // Return all of the values of the object's own properties.
  // Ignore functions
  // http://underscorejs.org/#values
} */

/* Objects.prototype.values = function(obj){

    let values = [];
    for (let i in obj) {
        values.push(obj[i]);
    }
  return values;

} */




/* function mapObject(obj, cb) {
  // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
  // http://underscorejs.org/#mapObject
} */


/* Objects.prototype.mapObject = function(obj, cb){

    let newObject = {};
    for (let i in obj) {
      newObject[i] = cb(obj[i], i);
    }
    return newObject;
  
  } */





 /*  function pairs(obj) {
  // Convert an object into a list of [key, value] pairs.
  // http://underscorejs.org/#pairs
} */


/* Objects.prototype.pairs = function(obj){

    let pair = [];
    for (let i in obj) {
        pair.push([i, obj[i]]);
    }
    return pair;
  
  } */




/* STRETCH PROBLEMS */

/* function invert(obj) {
  // Returns a copy of the object where the keys have become the values and the values the keys.
  // Assume that all of the object's values will be unique and string serializable.
  // http://underscorejs.org/#invert
} */


/* Objects.prototype.invert = function(obj){

    let invertObject = {};
    for (let i in obj) {
        invertObject[obj[i]] = i;
    }
    return invertObject;
  
}  */





/* 
function defaults(obj, defaultProps) {
  // Fill in undefined properties that match properties on the `defaultProps` parameter object.
  // Return `obj`.
  // http://underscorejs.org/#defaults
} */

/* 
Objects.prototype.defaults = function(obj, defaultProps){
    for (let i in defaultProps) {
  
        if(obj[i] === undefined){
            obj[i] = defaultProps[i];
        }
      
    }
    return obj;
  
} */



module.exports=Objects;

