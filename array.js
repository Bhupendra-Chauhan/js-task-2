/*function each(elements, cb) {
  // Do NOT use forEach to complete this function.
  // Iterates over a list of elements, yielding each in turn to the `cb` function.
  // This only needs to work with Arrays.
  // You should also pass the index into `cb` as the second argument
  // based off http://underscorejs.org/#each
}*/


const Arrays = function(){

}

 Arrays.prototype.each = function(elements, cb){

  for (let i = 0; i < elements.length; i++) {
    
    cb(elements[i], i, elements);

  }
} 


/* function map(elements, cb) {
  // Do NOT use .map, to complete this function.
  // How map works: Map calls a provided callback function once for each element in an array, in order, and function acts as a new array from the res .
  // Produces a new array of values by mapping each value in list through a transformation function (iteratee).
  // Return the new array.
} */

 Arrays.prototype.map=function(elements, cb){
    let arr=[];
    for(let i=0;i<elements.length;i++){
        let element=cb(elements[i],i,elements);
        arr.push(element);
    }
    return arr;
}




/* function reduce(elements, cb, startingValue) {
  // Do NOT use .reduce to complete this function.
  // How reduce works: A reduce function combines all elements into a single value going from left to right.
  // Elements will be passed one by one into `cb` along with the `startingValue`.
  // `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
  // `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the initial value.
} */



 Arrays.prototype.reduce = function(elements, cb, startingValue){
  
  let index = 0;

  if(startingValue == undefined){
    index = 1;
    startingValue = elements[0];
  }

  for (let i = index; i < elements.length; i++) {

    startingValue = cb(startingValue, elements[i], i, elements);

  }
  return startingValue;

}  





/* function find(elements, cb) {
  // Do NOT use .includes, to complete this function.
  // Look through each value in `elements` and pass each element to `cb`.
  // If `cb` returns `true` then return that element.
  // Return `undefined` if no elements pass the truth test.
} */




 Arrays.prototype.find = function(elements, cb){

  let element_find;
  if(elements == undefined || !Array.isArray(elements) || cb == undefined){
    return undefined;
  }

  for (let i = 0; i < elements.length; i++) {

    let isFound = cb(elements[i], i, elements);

    if(isFound){

      element_find = elements[i];

      break; 
    }

  }

  return element_find;

} 




/* function filter(elements, cb) {
  // Do NOT use .filter, to complete this function.
  // Similar to `find` but you will return an array of all elements that passed the truth test
  // Return an empty array if no elements pass the truth test
} */




 Arrays.prototype.filter = function(elements, cb){

  let found_elements = [];
  if(elements == undefined || !Array.isArray(elements) || cb == undefined){
    return [];
  }

  for (let i = 0; i < elements.length; i++) {

    let isElementFound = cb(elements[i], i, elements);

    if(isElementFound){

      found_elements.push( elements[i] );

    }

  }

  return found_elements;
}





/* const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'

function flatten(elements) {
  // Flattens a nested array (the nesting can be to any depth).
  // Hint: You can solve this using recursion.
  // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
} */




Arrays.prototype.flatten = function (elements, depth = Infinity) {

  if(depth > 0){
    let flatArray = this.reduce(elements, (accumulator , currentValue) => 
    {
      let concatValue;

      if (Array.isArray(currentValue)) {

        concatValue = this.flatten(currentValue, depth - 1);
      }
      else{
        concatValue = currentValue;
      }

    return accumulator.concat(concatValue);

    }, []);
    return flatArray;
  }
  else
  {
    return elements.slice();
  }
  
}


module.exports=Arrays;